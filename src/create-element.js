module.exports = {
	async create(elementtype, append, style, source) {
		var x = document.createElement(elementtype)
        if(style !== undefined) {
            x.style = style
        }
        if(source !== undefined) {
            x.src = source
        }
        if(append !== undefined) {
            append.appendChild(append);
        }
	},
};
